
import java.util.ArrayList;
import java.util.List;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.JobFlowInstancesConfig;
import com.amazonaws.services.elasticmapreduce.model.*;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowResult;
import com.amazonaws.services.elasticmapreduce.util.StepFactory;


public class Cluster {
	
	static String V471 = "emr-4.7.1";
	static String TIPODEFECTO = "m3.xlarge";
	
	public static void main (String arg[]){
		
		/*RunJobFlowResult resultado;
		Cluster cluster = new Cluster();
		resultado = cluster.crearCluster("Cluster Final Java",cluster.V471,"PruebaSpark", 10, cluster.TIPODEFECTO);
		System.out.println("toString: " + resultado.toString());
		System.out.println("getJobFlowId: " + resultado.getJobFlowId());*/
		
		
		Cluster cluster = new Cluster();
		cluster.lanzarTarea();
	}
	
	public void lanzarTarea(){
		
	 
		StepFactory stepFactory = new StepFactory();
		AmazonElasticMapReduceClient emr = new AmazonElasticMapReduceClient();
		AddJobFlowStepsRequest req = new AddJobFlowStepsRequest();
		req.withJobFlowId("j-BZ3F2MEAMEG2");

		List<StepConfig> stepConfigs = new ArrayList<StepConfig>();
				
		HadoopJarStepConfig sparkStepConf = new HadoopJarStepConfig()
					.withJar("command-runner.jar")
					.withArgs("spark-submit","--executor-memory","1g","--class","org.apache.spark.examples.SparkPi","/usr/lib/spark/lib/spark-examples.jar","10");			
				
		StepConfig sparkStep = new StepConfig()
					.withName("Spark Step")
					.withActionOnFailure("CONTINUE")
					.withHadoopJarStep(sparkStepConf);

		stepConfigs.add(sparkStep);
		req.withSteps(stepConfigs);
		AddJobFlowStepsResult result = emr.addJobFlowSteps(req);
		System.out.println("toString: " + result.toString());
	}
	
	public RunJobFlowResult crearCluster(String nombreCluster, String versionERM, String keyPairs,
			int numeroInstancias, String tipoInstancia){
		
		AmazonElasticMapReduceClient emr = new AmazonElasticMapReduceClient();
		List<Application> myApps = new ArrayList<Application>();
		Application sparkApp = new Application()
		    .withName("Spark");
		myApps.add(sparkApp);
		
		RunJobFlowRequest request = new RunJobFlowRequest()
		    .withName(nombreCluster)
		    .withApplications(myApps)
		    .withReleaseLabel(versionERM)
		    .withLogUri("s3://sparkuniandes/log-elasticmapreduce/")
		    .withInstances(new JobFlowInstancesConfig()
		    .withEc2KeyName(keyPairs)
		    .withInstanceCount(numeroInstancias)
		        .withKeepJobFlowAliveWhenNoSteps(true)
		        .withMasterInstanceType(tipoInstancia)
		        .withSlaveInstanceType(tipoInstancia)
		    );		
		request.setServiceRole("EMR_DefaultRole");
		request.setJobFlowRole("EMR_EC2_DefaultRole");
		RunJobFlowResult result = emr.runJobFlow(request);
		return result;
		
	}

}
